#!/bin/bash

biblio=$(grep 'bibliox' ../chemins.tex | grep -Po '(?<=\{).*(?=\})')
bibimg=$(grep 'imgbiblio' ../chemins.tex | grep -Po '(?<=\{).*(?=\})')
img=$(grep 'chemimage' ../chemins.tex | grep -Po '(?<=\{).*(?=\})')

tex=$(echo "\\documentclass{proj}
\\begin{document}"
for i in "$@"; do
echo "\\iproj{${i%.*}}"
done
echo "\\end{document}"
)
echo $tex > $HOME/scripts/.temp/proj.tex &&
latexmk -f -outdir=$HOME/scripts/.temp/ -pdf $HOME/scripts/.temp/proj.tex  
mv $HOME/scripts/.temp/proj.pdf $HOME/Bureau/ &&
rm -f $HOME/scripts/.temp/*.*
