#!/bin/bash
rsync -av --update --exclude-from=/home/$USER/scripts/to_oc_exclude.txt /home/$USER/Documents/histoire /home/$USER/Nextcloud
rsync -av --update --exclude-from=/home/$USER/scripts/to_oc_exclude.txt /home/$USER/Documents/sociologie /home/$USER/Nextcloud
rsync -av --update --exclude-from=/home/$USER/scripts/to_oc_exclude.txt /home/$USER/Documents/philo /home/$USER/Nextcloud
rsync -av --update --exclude-from=/home/$USER/scripts/to_oc_exclude.txt /home/$USER/Documents/Montbrelloz /home/$USER/Nextcloud2
rsync -av --update --exclude-from=/home/$USER/scripts/to_oc_exclude.txt /home/$USER/Documents/CLASSES /home/$USER/Nextcloud
rsync -av --update --exclude-from=/home/$USER/scripts/to_oc_exclude.txt /home/$USER/Nextcloud/histoire /home/$USER/Documents
rsync -av --update --exclude-from=/home/$USER/scripts/to_oc_exclude.txt /home/$USER/Nextcloud/sociologie /home/$USER/Documents
rsync -av --update --exclude-from=/home/$USER/scripts/to_oc_exclude.txt /home/$USER/Nextcloud/philo /home/$USER/Documents
rsync -av --update --exclude-from=/home/$USER/scripts/to_oc_exclude.txt /home/$USER/Nextcloud2/Montbrelloz /home/$USER/Documents
rsync -av --update --exclude-from=/home/$USER/scripts/to_oc_exclude.txt /home/$USER/Nextcloud/CLASSES /home/$USER/Documents
