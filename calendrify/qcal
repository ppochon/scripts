#!/bin/bash
# qcal pose des question et produit pour chaque classe un fichier
# cours_$classe.md ainsi qu'un fichier calendrier_$annee.ics 
# Requiert le fichier vacances.csv dans le même répertoire
# Requiert Gawk
# 
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"
listvac="$SCRIPTPATH/vacances.csv"

while true # On demande l'année scolaire en vérifiant le chiffre
  do       # et on va chercher la rentrée et la fin dans vancances.csv
    read -p "Année scolaire (rentrée, ex. 2019): " anneeScol
    debAnnee="$(grep "$anneeScol" $listvac | grep 'Rentrée' | awk -F ',' '{ printf($1) }')"
    finAnnee="$(grep "$((anneeScol+1))" $listvac | grep 'Fin de' | awk -F ',' '{ printf($1) }')"
    if [[ $anneeScol =~ 2[0-9][1-9][0-9] ]]
      then break
    fi
  done

cyn=o # variable classe suplémentaire à oui 
pyn=o # variable période supplémentaire à oui
cc=0  # un compteur pour les classes

while [ "$cyn" = "o" ]
  do 
    cc=$[cc + 1]            # ce sera la classe n
    pc=0                    # reset du compteur pour les periodes
    da=$debAnnee            # reset de l'année de départ
    read -p "Nom de la classe $cc (ex. 1m12): " classe 
    listeClasse+=( "$classe" )

    while [ "$pyn" = "o" ]  # si période suplémentaire on enregistre
      do
        pc=$[pc + 1]        # ce sera la période n
        PS3="Numéro du jour de la semaine (ex. 3): "
        options=("lundi" "mardi" "mercredi" "jeudi" "vendredi")
        select opt in "${options[@]}"
          do
            case $opt in
              "lundi")    jsem=1 ; break ;;
              "mardi")    jsem=2 ; break ;;
              "mercredi") jsem=3 ; break ;;
              "jeudi")    jsem=4 ; break ;;
              "vendredi") jsem=5 ; break ;;
              *) echo "Entrée invalide: $REPLY."
                 echo "Entrez un chiffre de 1 à 5.";;
            esac
          done
        while true          # heure du début avec vérification
          do
            read -p "Début de la période $pc (ex. 08:15): " dper
            if [[ $dper =~ [0-2][0-9]:[0-5][0-9] ]]
              then break
            else echo "Entrée invalide: $dper."
                 echo "Utilisez le format 10:55"
            fi
          done
        while true          # heure de fin avec vérification
          do
            read -p "Fin de la période (ex. 09:00): " fper
            if [[ $fper =~ [0-2][0-9]:[0-5][0-9] ]]
              then break
            else echo "Entrée invalide: $dper."
                 echo "Utilisez le format 10:55"
            fi
          done
        read -p "Salle pour cette période (ex. F206): " salle
        
        # ajout des var dans des arrays
        jourSemaine+=( "$jsem" )
        debPeriode+=( "$dper" )
        finPeriode+=( "$fper" )
        salles+=( "$salle" )
        
        while true          # oui ou non une autre période?
          do
            read -p "Autre période pour la $classe ? (o/n): " pyn
            if [[ $pyn =~ [on] ]]
              then break
            fi
          done
      done 

    # On a ce qu'il faut pour produire cours_$classe.md

    while [ "$da" != "$finAnnee" ]        # boucle pour produire le .md
      do                                  # jour par jour (+1 avant done)
      jour=$(date -d "$da" "+%u")         # chiffre du jour de la semaine
      c=0                                 # conteur de tour
      for i in "${jourSemaine[@]}"
        do
        if [ "$jour" = "$i" ]  ; then     # si c'est un jour de cours
          x=$(date -d "$da" "+%a %d %b %Y")                  
          if grep -liq "$da" $listvac ; then   # et si c'est vacances
            vac="$(grep $da $listvac | awk -F ',' '{ printf($2) }')" 
            echo $x $vac                  # date et vacance
          else
            echo $x ${debPeriode[$c]}-${finPeriode[$c]} ${salles[$c]} 
          fi
        fi
        c=$[c + 1]
        done
        da=$(date -I -d "$da + 1 day")
      done > cours_"$classe".md &&
    sed -i "1s/^/$classe\n/" cours_"$classe".md &&
    echo "Classe $classe enregistrée dans cours_"$classe".md" 
    pyn=o                   # recette du compteur des périodes
    jourSemaine=()          # on reset des listes pr proch classe
    debPeriode=()
    finPeriode=()
    salles=()
    while true              # on veut oui ou non
      do
        read -p "Autre classe ? (o/n): " cyn
        if [[ $cyn =~ [on] ]]
          then break
        fi
      done
  done
 
creat=$(date +%Y%m%dT%H%M00Z) # la date de création c'est maintenant
# sed pour changer le nom des mois en chiffres et détacher les heures
sed -e 's/\sjan\s/ 01 /' -e 's/\sfév\s/ 02 /' -e 's/\smar\s/ 03 /' \
-e 's/\savr\s/ 04 /' -e 's/\smai\s/ 05 /' -e 's/\sjun\s/ 06 /' \
-e 's/\sjui\s/ 07 /' -e 's/\saoû\s/ 08 /' -e 's/\ssep\s/ 09 /' \
-e 's/\soct\s/ 10 /' -e 's/\snov\s/ 11 /' -e 's/\sdéc\s/ 12 /' \
-e 's/\([0-9][0-9]\):\([0-9][0-9]\)-\([0-9][0-9]\):\([0-9][0-9]\)/\1 \2 \3 \4/' cours_*.md |
# mawk 1.1.3 (défaut sur debien/ubuntu) ne comprend pas les extensions de caractère [:space:] etc. On utilise un space littéral pour  l'instant.
awk -v creat="$creat" ' BEGIN {
    ORS="\r\n"

    print "BEGIN:VCALENDAR"
    print "PRODID:-//gitlab.com/ppochon/scripts/blob/master/calendrify/qcal"
    print "VERSION:2.0"
    print "BEGIN:VTIMEZONE"
    print "TZID:Europe/Zurich"
    print "BEGIN:DAYLIGHT"
    print "TZOFFSETFROM:+0100"
    print "TZOFFSETTO:+0200"
    print "TZNAME:CEST"
    print "DTSTART:19700329T020000"
    print "RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=3"
    print "END:DAYLIGHT"
    print "BEGIN:STANDARD"
    print "TZOFFSETFROM:+0200"
    print "TZOFFSETTO:+0100"
    print "TZNAME:CET"
    print "DTSTART:19701025T030000"
    print "RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=10"
    print "END:STANDARD"
    print "END:VTIMEZONE" 
    }
/^[^ ]+$/ { cls=$1 }         # Récup. du nom de la classe
$1~/^(lun|mar|mer|jeu|ven)$/ && $5~/^[0-9]+/ { 
    cmd="uuidgen"
    cmd|getline uuid         # stockage du rés. de la com. dans uuid
    prtEndVevent()
    
    lastmod = "20170317T091634Z"
    dtstamp = "20170317T091634Z"
    date = $4 $3 $2
    begt = $5 $6 "00"
    endt = $7 $8 "00"
    room = $9
    
    print "BEGIN:VEVENT"
    print "CREATED:" creat
    print "LAST-MODIFIED:" creat 
    print "DTSTAMP:" creat
    print "UID:"uuid""
    print "SUMMARY:" cls
    print "DTSTART;TZID=Europe/Zurich:" date "T" begt 
    print "DTEND;TZID=Europe/Zurich:"   date "T" endt
    print "LOCATION:" room
    print "TRANSP:OPAQUE"
    close(cmd)
    next
    }
/^[ \t]+/ {        # /^[^[:space:]]+$/ serait mieux
    gsub(/^[ \t]+|[ ]+$/,"")
    desc = (desc == "" ? "DESCRIPTION:" : desc RS) $0
    }
END {
    prtEndVevent()
    print "END:VCALENDAR"
    }

function prtEndVevent(wid) {
    if ( desc != "" ) {
        wid = 74
        gsub(RS,"\\n",desc)
        while ( desc !~ /^ ?$/ ) {
            print substr(desc,1,wid)
            desc = " " substr(desc,wid+1)
        }
        desc = ""
    }
    if ( endVevent != "" ) {
        print endVevent
    }
    endVevent = "END:VEVENT"
}' > calendrier_"$anneeScol".ics
echo "Le fichier calendrier_$anneeScol.ics a été créé."
