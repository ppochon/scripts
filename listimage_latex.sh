#!/bin/bash

biblio=$(grep 'bibliox' ../chemins.tex | grep -Po '(?<=\{).*(?=\})')
bibimg=$(grep 'imgbiblio' ../chemins.tex | grep -Po '(?<=\{).*(?=\})')
img=$(grep 'chemimage' ../chemins.tex | grep -Po '(?<=\{).*(?=\})')

#prompt pour le titre etc
read -e -p 'entrez le titre: ' titre
read -e -p 'entrez le soustitre: ' soustitre

echo "\\documentclass[11pt, a4paper]{article}
\\usepackage[frenchb]{babel}
\\usepackage[ibidtracker=false,backend=biber,citestyle=verbose-trad2,bibstyle=verbose-trad3,url=true]{biblatex} 
\\addbibresource{$biblio}
\\addbibresource{$bibimg}
\\usepackage{graphicx}
\\usepackage[expert]{fourier}
\\usepackage[utf8]{inputenc}
\\usepackage[babel=true]{microtype}
\\usepackage{ragged2e}
\\usepackage{csquotes}
\\usepackage{xspace}
\\usepackage{datetime}
    \\renewcommand{\\newunitpunct}[0]{\\addcomma\\addspace}
\\DeclareFieldFormat[thesis]{title}{\\textbf{#1}}
\\DeclareBibliographyDriver{thesis}{%
    \\usebibmacro{bibindex}%
    \\usebibmacro{begentry}%
    \\usebibmacro{title}%
    \\newunit\\newblock
    \\usebibmacro{author}%
    \\printfield{addendum}
    \\newunit\\newblock
    \\printfield{type}%
    \\newunit\\newblock
    \\usebibmacro{institution+location+date}%
    \\newunit\\newblock
    \\printfield{pubstate}
    \\newunit\\newblock
    \\usebibmacro{doi+eprint+url}
}

\\renewbibmacro*{date}{%
    \\printdate
    \\iffieldundef{origyear}%
    {}%
    {%
    \\setunit*{\\addspace}%
    \\printtext[parens]{\\printorigdate}%
    }%
}
\\usepackage{petitesmacros} 
\\graphicspath{{$img}} 
\\usepackage{url}
\\usepackage{hyperref} 
\\hypersetup{breaklinks=true} 

\\begin{document}
\\date{}
\\title{%
    \\begin{minipage}\\linewidth
        \\RaggedRight{%
        \\texttitle{$titre}
        \\vskip3pt
        \\large $soustitre
        \\vskip6pt
        Le \\today
        \\vskip3pt
        Signaler une erreur:  \\href{mailto:pierre.pochon@vd.educanet2.ch?subject=ERREUR dans le fichier \\jobname}{\\texttt{pierre.pochon@vd.educanet2.ch}}}
    \\end{minipage}
}

\\maketitle
"

for i in $img*.{jpg,pdf}
do
    i=${i##*/}
    echo "\\noindent\\begin{minipage}[c]{.5\\linewidth}
    \\includegraphics[width=\\linewidth]{${i}}
    \\end{minipage}\\hfill
    \\begin{minipage}[c]{.35\\linewidth}
    \\cite{${i%.*}}\\smallskip
    \\end{minipage}"
done

echo "\\end{document}"


