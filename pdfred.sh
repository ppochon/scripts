#!/bin/bash
# reduit le pdf voir: http://cupnet.net/reduce-pdf-size/

# veridier l'existence d'un argument

if [ $# -eq 0 ]
then
    echo "Veuillez entrer le nom du fichier"
    exit
fi

# vérifier si le fichier existe
if [ ! -e $1 ]
then
    echo "Le fichier n'existe pas"
    exit
fi

input=$1
output=${input%pdf}rdt.pdf

gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/default -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$output $input

# print taille des fichiers
du -sh $input
du -sh $output

