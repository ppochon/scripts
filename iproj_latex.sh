#!/bin/bash

biblio=$(grep 'bibliox' ../chemins.tex | grep -Po '(?<=\{).*(?=\})')
bibimg=$(grep 'imgbiblio' ../chemins.tex | grep -Po '(?<=\{).*(?=\})')
img=$(grep 'chemimage' ../chemins.tex | grep -Po '(?<=\{).*(?=\})')

#prompt pour le titre etc
read -e -p 'entrez le surtitre: ' surtitre
read -e -p 'entrez le titre: ' titre
read -e -p 'entrez obligatoirement le soustitre: ' soustitre

mogrify -density 150 $img*.jpg

echo "\\documentclass{proj}
\\begin{document}
\\surtitre{$surtitre}%
\\titre{$titre}%
\\soustitre{$soustitre}%
\\titrage
"

for i in $img*.{jpg,pdf}
do
    i=${i##*/}
    echo "\\iproj{${i%.*}}"
done

echo "\\end{document}"


