#!/bin/bash

rsync -arv --del --backup --suffix=.~sauv --force --ignore-errors --exclude 'Téléchargements' --exclude 'Vidéos' --exclude 'VirtualBox VMs' --exclude '.dbus' --exclude '.gvfs' --exclude '.cache/' --exclude '/.thumbnails' --exclude 'ownCloud' /home/$USER /media/$USER/histoire1
