#!/bin/bash

rsync -arvP /mnt/NAS/films_1/VIDEOS_histoire/ /media/$USER/Mercury/films_histoire
rsync -arvP /mnt/NAS/films_2/VIDEOS_histoire_2/ /media/$USER/Mercury/films_histoire
rsync -arvP /mnt/NAS/films_2/VIDEOS_philo/ /media/$USER/Mercury/films_philo
rsync -arvP /mnt/NAS/films_2/CINE_FICTION/ /media/$USER/Mercury/films_fiction
