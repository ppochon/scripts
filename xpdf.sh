#!/bin/sh

read -p "Input USER Password : " usrpwd

read -p "Input OWNER Password : " ownerpwd

for file in *.pdf 
do 
  qpdf --encrypt $usrpwd $ownerpwd 256 -- "$file" "${file%.pdf}.x.pdf" 
done

echo "Files are completed"
