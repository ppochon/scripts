#!/bin/bash
# Bash Menu Script Example

PS3='Please enter your choice: '
options=("Option 1" "Option 2" "Option 3" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Option 1")
            echo "you chose choice 1"
            liste+=( "$REPLY" )
            ;;
        "Option 2")
            echo "you chose choice 2"
            liste+=( "$REPLY" )
            ;;
        "Option 3")
            echo "you chose choice $REPLY which is $opt"
            liste+=( "$REPLY" )
            ;;
        "Quit")
            echo "${liste[@]}"
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
#pour test
