.*
*.aux
*.bbl
*.bcf
*.blg
*.fdb_latexmk
*.fls
*.log
*.nav
*.out
*.run.xml
*.snm
*.swp
*.toc
*.vrb
*.bib.bak
*.synctex.gz
*.sav
*FILMS*

